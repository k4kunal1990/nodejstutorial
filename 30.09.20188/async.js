var express = require('express');
var app = express();
var async = require('async');

var array = ["kunal", "gopal", "harry", "john", "ron"];
async.each(array, function(data, cb){
	if(data){
		console.log(data);
	}else{
		console.log('err');
	}
	cb();
})
var result = [];
result.users = {};
result.dept = {};
async.series([
function(cb){
	User.find().exec(function(err, user){
		if(err){
			cb();
		}
		if(user){
			result.users = users; 
			cb();
		}
	})
},
function(cb){
	Department.find().exec(function(err, dept){
		if(err){
			cb();
		}
		if(user){
			result.dept = dept; 
			cb();
		}
	})
}

], function(err, result){
		if(err){
			console.log(err);
			res.json({"err" : err});
		}
		if(result){
			res.json({"data" : result});
		}
})

// waterfall

var result = [];
result.users = {};
result.dept = {};

async.waterfall([
	function(cb){
		User.findOne({_id : 1}).exec(function(err, user){
			if(err){
				cb(err, null);
			}
			if(user){
				result.user = user;
				cb(null, result);
			}
		})
	},
	function(arg,cb){
		if(arg){
			Department.find({user_id : arg.user_id}).exec(function(err, dept){
				if(err){
					cb(err, null);
				}
				if(dept){
					result.dept = dept;
					cb(null, result);
				}
			})
		}
	}
], function(err, result){
	if(err){
		console.log(err);
		res.json({"err" : err});
	}
	if(result){
		res.json({"data" : result});
	}
})





app.listen(3000, function(){
	console.log('connected!!');
})


