var express = require('express');
var mongoose = require('mongoose');
var db = mongoose.connect('mongodb://<username>:<password>@<hostname>:<port>/<database>', { "useNewUrlParser" : true});
var student = require('./students');
var city = require('./city');
var state = require('./state');
var app = express();

app.get('/getStudent', function(req, res){
	city.aggregate([
		// {
			// $match : {
				// name : "kunal"
			// }
		// },
		// {
			// $project : {
				// _id : 0,
				// studentname : "$name",
				// studentmarks : "$marks"
			// }
		// },
		// {
			// $unwind : { path: "$subjects", includeArrayIndex : "subjectArrayIndex"}
		// }
		
		// {
			// $group : {
				// "_id" : {
					// "Section" : "$section"
				// },
				// "TotalMarksObtains" : {
					// $sum : "$marks"
				// },
				// "Average" : {
					// $avg : "$marks"
				// }
			// }
		// },
		// {
			// $sort : {
				// "Section" : -1
			// }
		// },
		// {
			// $skip : 1
		// },
		// {
			// $limit : 2
		// }
		{
			$lookup : {
				from : "state",
				localField : "stateId",
				foreignField : "stateId",
				as : "stateData"
			}
		},
		{
			$project : {
				City : "$name",
				Result : {
					$cond : {if : {$eq : ["$stateId", 1]}, then : "My native", else : "Not my Native"}
				}
			}
		}
		
	]).exec(function(err, data){
		if(err){
			console.log(err);
			res.json({"data" : err});	
		}else{
			console.log(data);
			res.json({"data" : data});	
		}
		
	})
})

app.listen(3001, function(){
	console.log('connected to port 3001');
})
