var mongoose = require('mongoose'),
    schema = mongoose.Schema;

var citySchema = schema({
    name : {
      type : String,
      required : true
    },
    stateId : {
      type : mongoose.Schema.ObjectId,
      ref : 'state'
    },
    country : {
      type : String,
      required : true
    }

});

module.exports = mongoose.model('city', citySchema, 'city');
