var mongoose = require('mongoose'),
    schema = mongoose.Schema;

var studentSchema = schema({
    name : {
      type : String,
      required : true
    },
    section : {
      type : String,
      required : true
    },
    marks : {
      type : Number,
      required : true
    },
	subjects : {
		type : Array
	}

});

module.exports = mongoose.model('students', studentSchema, 'students');
