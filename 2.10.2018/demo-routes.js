var express = require('express');
var router = express.Router();
var cont = require('./export');

router.get('/test1', cont.getdata);
router.get('/test2', cont.getUpdatedData);

module.exports = router;