var mongoose = require('mongoose'),
    schema = mongoose.Schema;

var stateSchema = schema({
    name : {
      type : String,
      required : true
    },
    stateId : {
      type : String,
      required : true
    }

});

module.exports = mongoose.model('state', stateSchema, 'state');
