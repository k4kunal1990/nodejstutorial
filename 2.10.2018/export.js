const versionRouter = require('express-version-route');


module.exports.getdata = function(req, res, next){
	console.log(req.version);
	return res.json({data : 'getdata1'});
}

module.exports.getUpdatedData = function(req, res, next){
	console.log(req.version);
	return res.json({data : 'getUpdatedData2'});
}
	
module.exports.wrapper = function(arg){
	const routesMap = new Map();	
	if(typeof arg === 'function'){
        routesMap.set('1.0.0', arg);
		console.log(routesMap);
		//console.log(arg.toString());
        return versionRouter.route(routesMap);
		//return routesMap;
    } else {
		console.log('req.version');
        //console.log(versionRouter.route(arg));
		return arg;
    }
}

module.exports.routeMiddleware = function(req, res, next){
	req.version = '1.0.0';
	next();
}

// module.exports = {
	// routesMap.set('1.0.0', (req, res, next) => {
	  // console.log(req.version);
	  // return res.status(200).json({'message': 'hello to you version 1.0'})
	// })
	// routesMap.set('2.0.0', (req, res, next) => {
		// console.log(req.version);
	  // return res.status(200).json({'message': 'hello to you version 2.0'})
	// })
	// routesMap.set('default', (req, res, next) => {
	  // return res.status(200).json({'message': 'hello to you, this is the default route'})
	// })
	
	
// }

