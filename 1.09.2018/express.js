var express = require('express');
var body = require('body-parser');
var cookieParser = require('cookie-parser');
var app = express();

app.use(body.json());

app.use(function(req, res, next){
	console.log('time for request ' + Date.now());
	console.log(req.headers);
	
})

app.get('/hello', function(req, res){
	console.log(req);
	res.send();
});
app.post('/login', function(req, res){
	console.log(req.body);
	console.log('logging post request');
	res.json({"result" : "response came"});
});

app.listen(3000, function(){
	console.log('connected to 3000');
});


// application level application
app.use(function(req, res, next){
	console.log('blahh blahh');
	next();
})

app.use(function(req, res, next){
	console.log('')
})


app.get('/', function(req, res, next){
	
})


// Router level middleware 

var router = express.Router();
router.post('/login', function(){
	
})


// Error handling middleware

app.use(function(err, req, res, next){
	console.log(err.stack);
	console.log(err.code);
	next();
})

// Built in middlewares

express.static();

express.urlencoded({extended : true});

// third party middleware

app.use(body.json());
app.use(cookieParser());