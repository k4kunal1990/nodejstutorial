// normal function 

function box(){
	console.log('hello');
}
box();

// global and local scope of function 
var a = "dog";

function box1(){
	var a = 'cat';
	console.log(a);
}

box1();
console.log(a);

// immediately executing function 
(function box2(){
	console.log("this is box 2");
})();

// anonymous function
var box3 = function(){
	console.log("hello world");
}

box3();

// higher order function 

setTimeout(function(){
	console.log("hello after 2 seconds");
}, 2000);


// closures 

function outFunc(a){
	var outvar = a;
	function innerFunc(){
		console.log(outvar);
	}
	innerFunc();
}

outFunc("hello kunal");