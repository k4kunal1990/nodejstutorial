var express = require('express'),
	mongo = require('mongodb').MongoClient,
	url = "mongodb://localhost:27017/kunal",
	app = express();
	
	mongo.connect(url, {useNewUrlParser : true}, function(err, db){
		if(err){
			console.log(err);
		}
		db.db('kunal').createCollection("users", function(err, res){
			if(err){
				console.log(err);
			}
			var newRow = {name : 'kunal', class : 10, address : 'bangalore'};
			db.db('kunal').collection('users').insertOne(newRow, function(err, data){
				if(err){
					console.log(err);
				}
				db.db('kunal').collection('users').findOne({}, function(err, res){
					if(err){
						console.log(err);
					}
					console.log(res);
				})
			})
			console.log('new collection added');
		})
		// console.log(db);
	})

	app.listen(3000, function(){
		console.log('connected to 3000');
	})