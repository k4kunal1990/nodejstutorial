var mongoose = require('mongoose'),
    schema = mongoose.Schema;
	
var userSchema = new schema({
	deptId : {
		type : mongoose.Schema.ObjectId,
		ref : 'departments'
	},
	email : {
		type : String,
		lowercase : true,
		trim : true,
		required : true
	},
	name : {
		type : String
	},
	username : String
	
})

module.exports = mongoose.model('user', userSchema, 'user');