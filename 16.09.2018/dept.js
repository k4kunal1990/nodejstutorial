var mongoose = require('mongoose'),
    schema = mongoose.Schema;
	
var deptSchema = new schema({
	name : {
		type : String,
		lowercase : true,
		trim : true,
		required : true
	},
	post : {
		type : String
	}
	
})

module.exports = mongoose.model('dept', deptSchema, 'dept');