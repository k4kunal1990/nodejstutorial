var express = require('express'),
	mongoose = require('mongoose'),
	db = mongoose.connect("mongodb://localhost:27017/kunal"),
	UserModel = require('./users');
	app = express();
	
	app.get('/users', function(req, res){
		// var dataObj = new UserModel();
		// dataObj.name = 'kunal';
		// dataObj.email = "KUNAL@GMAIL.com";
		// dataObj.username = 'kunal';
		
		// dataObj.save(function(err, saved, affected){
			// if(err){
				// console.log(err);
			// }
			// console.log(saved);
			// console.log(affected);
		// })
		
		// UserModel.find({email : 'kunal@gmail.com'}).populate('deptId').exec(function(err, data){
			// if(err){
				// res.json(err);
			// }
			// res.json(data);
		// })
		
		UserModel.aggregate([
			{
				$lookup : {
					from : "dept",
					localField : "deptId",
					foreignField : "_id",
					as : "department_data"
				}
			}
			// $project : {
				// name : 1,
				// email : 1,
				// deptId : 1,
				// email_data : {
					// $filter : {
						// input : "$email",
						// as : "$email",
						// cond : {
							// $eq : [email , 'kunal@gmail']
						// }
					// }
				// }
			// }
			
		
		]).exec(function(err, data){
			if(err){
				res.json(err);
			}
			res.json(data);
		})
		
		
		
	})
	
	app.put('/users', function(req, res){
		var dataObj = {email : 'gopal@gmail.com', username : 'gopal', name : 'gopal'};
		var condition = {_id : '5b9e368716b4901708dec651'};
		UserModel.findOneAndUpdate(condition, dataObj, {new : true}, function(err, updated){
			if(err){
				res.json(err);
			}
			if(updated){
				res.json(updated);
			}
		})
	})
	
	app.delete('/users', function(req, res){
		UserModel.findOneAndRemove({_id : '5b9e368716b4901708dec651'}, function(err, data){
			if(err){
				res.json(err);
			}
			if(data){
				res.json(data);
			}
		})
	})

	app.listen(3000, function(){
		console.log('connected to 3000');
	})